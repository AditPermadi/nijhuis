<?php require 'header_contact.php';?>

<section class="main_content">
        
        <div class="content">

            <div class='services'>
                <p class='h1' id='services'>Brandwacht</p>
            </div>
            
            <section class="twee_kolommen_content">
                <div class="twee_kolommen_section">
                    <p class="h3">Brandveiligheid</p>

                    <div class="sectie_text">
                        <p>
                            Als brandwacht hou ik toezicht op de brandveiligheid en algemene veiligheid van de mensen die namens de contractoren de werkzaamheden uitvoeren.
                            Als je gefocust bent op je werk dan heb je geen oog meer voor wat er om je heen gebeurt. 
                            Eventuele factoren in de buurt die mogelijk een brand kunnen veroorzaken neem je meteen weg voordat je de werkzaamheden laat starten. 
                            Door mijn aanwezigheid worden mogelijke onveilige situaties tot een minimum beperkt. 
                        </P>
                    </div>

                    <a class="button_1" href="./contact.php">Neem Contact Op</a>
                </div>

                <div class="twee_kolommen_section">
                    <div class="sectie_img_ph">
                        <img class="sectie_img" src="assets/img/2.jpg" alt="">
                    </div>
                </div>
            </section>

            <div class="terugknop_ph">
                <a href="./index.php" class="terugknop">Terug naar homepagina</a>
            </div>
        </div>
</section>

<?php require 'footer.php';?>