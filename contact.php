<?php require 'header_contact.php';?>

    <section class='main_content'>
        <section class="grid_content">
            <div class="contact_form">
                <h2>Contactformulier</h2>
                    <form action="">
                        <input type="text" name="voornaam" id="voornaam" placeholder='Voornaam*'>
                        <input type="text" name="achternaam" id="achternaam" placeholder="Achternaam*">
                        <input type="email" name="email" id="email" placeholder="Email*">
                        <textarea name="bericht" id="bericht" cols="30" rows="10" placeholder="Plaats hier uw bericht"></textarea>
                        <input type="submit" value="Verzenden">
                    </form>
            </div>

            
            <div>
                <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Donec metus lacus, aliquam et commodo eget, vulputate in erat.
                Praesent nec sodales mi. Donec in luctus ligula. Fusce pretium, 
                ipsum nec porta tincidunt, felis nunc fringilla neque, in interdum velit nulla 
                vel sapien. Fusce a sollicitudin nulla. Etiam ipsum urna, sollicitudin sit amet 
                lectus sit amet, rhoncus lobortis urna. Morbi ac. 
                </p>
            </div>
        </section>
    </section>

<?php require 'footer.php';?>