<footer>

    <div class="footer_contact">
        <table>
            <tr>
                <td>
                    <p class="footer_text" style="font-size: 18px; font-weight: bold;">
                        Nijhuis Safety Service
                    </p>
                </td>
            </tr>

            <tr>
                <td>
                    <p class="footer_text">
                        Professor Beelhoek 59
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p class="footer_text">
                        4908 CW  Oosterhout
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p class="footer_text">
                        +31 (0)6 – 29 03 85 12
                    </p>
                </td>
            </tr>
        </table>
    </div>
    <div class="site_info">
        <p class="footer_text">Nijhuis Safety Service @ 2020</p>
    </div>

</footer>

</body>


</html>