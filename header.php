<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/style.css">
    <title>Nijhuis Safety & Security</title>
</head>

<header id="front_page_header">

    <div class="header_content">

        <div class="menu_placeholder">
            <span class="menu">
                <span class="menu_items"><a href="./index.php">Home</a></span>
                <span class="menu_items"><a href="./contact.php">Contact</a></span>
            </span>
        </div>
    </div>

    <div class="moto">
        <p style="color:white; font-size:1.8em; font-style:italic; text-align:center;">“We werken veilig of we werken niet.”</p>
    </div>

    <div class="button_contact">
        <a class="button_1" href="./contact.php">Neem Contact Op</a>
    </div>
        
    

    
</header>

<body>