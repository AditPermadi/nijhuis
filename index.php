<?php require 'header.php';?>

    <section class="main_content">

        <div class="content">

            <div class='services'>
                <p class='h1' id='services'>Services</p>
            </div>

            <section class="grid_content">
                <section class="content_column" id='item_1'>
                        <div class="sectionimg_ph">
                            <img class="sectionimg" src="assets/img/2.jpg">
                        </div>
                        <a href='./brandwacht.php' class="maincontent_button" id='brandwacht'> Brandwacht </a>
                </section>

                <section class="content_column" id='item_2'>

                    <div class="sectionimg_ph">
                        <img class="sectionimg" src="assets/img/7.jpg">
                    </div>
                    <a href='./mangatwacht.php' class="maincontent_button" id='mangatwacht'> Mangatwacht </a>
                </section>

                <section class="content_column" id='item_3'>

                    <div class="sectionimg_ph">
                        <img class="sectionimg" src="assets/img/15.jpeg">
                    </div>
                    <a href='./veiligheidswacht.php' class="maincontent_button" id='veiligheidswacht'> Veiligheidswacht </a>
                </section>
            </section>

            <section class="twee_kolommen_content">
                <div class="twee_kolommen_section">

                    <p class="h1"> Over mij</p>

                    <p class="sectie_tekst">
                        In 2012 ben ik gestart als brandwacht in de petrochemische industrie. 
                        In de loop der jaren heb ik veel ervaring opgedaan op raffinaderijen, chemische fabrieken, energiecentrales, afvalverwerkingsbedrijven en diverse andere industrieën. 
                        Sinds 3 jaar ben ik als ZZP-er gaan werken omdat ik op deze manier nog meer de nadruk kan leggen op kwaliteit. Op ieder locatie zijn de omstandigheden en de regels anders. 
                        Het is van groot belang om kwalitatief hoogwaardig werk te leveren, waardoor eventuele opdrachtgevers eerder geneigd zijn om voor mij te kiezen i.p.v. iemand anders. 
                        Mijn ervaring ligt voornamelijk op het toezicht houden bij risicovolle werkzaamheden. Iedereen wil tenslotte weer veilig en gezond naar huis.
                    </p>
                </div>

                <div class="twee_kolommen_section">
                    <div class="sectie_img_ph">
                        <img class='over_mij_img' src="assets/img/16.jpeg">
                    </div>
                </div>
            </section>

        </div>

    </section>

<?php require 'footer.php';?>