<?php require 'header_contact.php';?>

<section class="main_content">
        
        <div class="content">

            <div class='services'>
                <p class='h1' id='services'>Mangatwacht</p>
            </div>
            
            <section class="twee_kolommen_content">
                <div class="twee_kolommen_section">
                    <p class="h3">Toezicht</p>

                    <div class="sectie_text">
                        <p>
                            Als mangatwacht houd je toezicht op een besloten ruimte. Een dergelijke ruimte is bij voorbaat al een zeer groot risico om in te werken. 
                            Zo zijn de vluchtwegen beperkt, is er een beperkte toegang en is de ventilatie vaak beperkt. 
                            Zo is het van essentieel belang dat er een correcte gasmeting wordt uitgevoerd voordat de medewerkers de besloten ruimte betreden. 
                            Zonder deze meting kunnen er doden vallen en dat is iets wat je natuurlijk niet wilt.
                            Van belang zijn ook de omstandigheden buiten de besloten ruimte omdat deze van invloed kunnen zijn op de omstandigheden in de besloten ruimte. 
                            Een correcte registratie van de medewerkers in de besloten ruimte is van belang bij een eventuele calamiteit. 
                            Je moet weten of alle medewerkers de ruimte hebben verlaten in een noodsituatie.
                        </P>

                        <p style="margin-top: 20px;">
                            Sinds kort houd ik ook toezicht op de besloten ruimte via CCTV.  
                            Dit is weer een totaal andere manier van toezicht houden. 
                            Omdat je op afstand toezicht houd is het van groot belang om nog waakzamer te zijn op details. 
                            Door de camera’s word je zicht op de omgeving beperkt waardoor het latiger word om de omgeving in de gaten te houden op eventuele veranderingen die van invloed kunnen zijn op de besloten ruimte.
                        </p>
                    </div>

                    <a class="button_1" href="./contact.php">Neem Contact Op</a>
                </div>

                <div class="twee_kolommen_section">
                    <div class="sectie_img_ph">
                        <img class="sectie_img" src="assets/img/7.jpg" alt="">
                    </div>
                </div>
            </section>

            <div class="terugknop_ph">
                <a href="./index.php" class="terugknop">Terug naar homepagina</a>
            </div>
        </div>
</section>

<?php require 'footer.php';?>