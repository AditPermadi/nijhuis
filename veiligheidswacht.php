<?php require 'header_contact.php';?>

<section class="main_content">
        
        <div class="content">

            <div class='services'>
                <p class='h1' id='services'>Mangatwacht</p>
            </div>
            
            <section class="twee_kolommen_content">
                <div class="twee_kolommen_section">
                    <p class="h3">Toezicht</p>

                    <div class="sectie_text">
                        <p>
                            Als veiligheidswacht houd je toezicht op de algemene veiligheid van de medewerkers van de contractoren. 
                            Je loopt area-rondes en je behoud het overzicht op de werkplek. Als veiligheidswacht zie je er op toe dat iedereen veilig werkt en de gereedschappen en PBM’s ook gebruikt waarvoor ze bedoeld zijn.
                        </P>

                        <p style="margin-top: 20px;">
                            Je kunt je werk natuurlijk niet naar behoren uitvoeren zonder een werkvergunning. 
                            Deze controleer je op de werkplek en je voert een LRMA uit om er zeker van te zijn dat al het werk veilig uitgevoerd kan worden.
                        </p>
                    </div>

                    <a class="button_1" href="./contact.php">Neem Contact Op</a>
                </div>

                <div class="twee_kolommen_section">
                    <div class="sectie_img_ph">
                        <img class="sectie_img" src="assets/img/15.jpeg" alt="">
                    </div>
                </div>
            </section>

            <div class="terugknop_ph">
                <a href="./index.php" class="terugknop">Terug naar homepagina</a>
            </div>
        </div>
</section>

<?php require 'footer.php';?>